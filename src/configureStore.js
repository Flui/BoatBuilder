import { createStore, combineReducers, compose } from 'redux';
import { reducer as formReducer } from 'redux-form';

const enhancer = compose(
  window.devToolsExtension ? window.devToolsExtension() : f => f,
);

const configureStore = () => {
  const persistedState = {
    form: {

    },
  };

  const reducers = {
    // app: appReducers,
    form: formReducer,
  };

  const reducer = combineReducers(reducers);
  const store = createStore(
    reducer,
    persistedState,
    enhancer,
  );

  return store;
};

export default configureStore;
