export const SUBMIT_VALUES = 'SUBMIT_VALUES';

export const submitValues = values => ({
  type: SUBMIT_VALUES,
  values,
});


export const UPDATE_SPRUNGHOEHE = 'UPDATE_SPRUNGHOEHE';
export const updateSprunghoehe = value => ({
  type: UPDATE_SPRUNGHOEHE,
  value,
});

export const UPDATE_BREITE = 'UPDATE_BREITE';
export const updateBreite = value => ({
  type: UPDATE_BREITE,
  value,
});

export const UPDATE_DECKSBALKENHOEHE = 'UPDATE_DECKSBALKENHOEHE';
export const updateDecksbalkenhoehe = value => ({
  type: UPDATE_DECKSBALKENHOEHE,
  value,
});

export const UPDATE_PRECISION = 'UPDATE_PRECISION';
export const updatePrecision = value => ({
  type: UPDATE_PRECISION,
  value,
});
