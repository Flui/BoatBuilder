import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import App from './App';

const muiTheme = getMuiTheme({});

const Root = ({ store }) => (
  <Provider store={store} >
    <MuiThemeProvider muiTheme={muiTheme} >
      <App />
    </MuiThemeProvider>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired, /* eslint react/forbid-prop-types: 0 */
};

export default Root;
