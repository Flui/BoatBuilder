import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import TextField from 'material-ui/TextField';
import * as actions from '../actions/form';

const renderTextField = props => (
  <TextField
    hintText={props.label}
    floatingLabelText={props.label}
    errorText={props.touched && props.error}
    {...props}
  />
);

renderTextField.propTypes = {
  label: PropTypes.string.isRequired,
  touched: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired,
};

const Form = ({ updateSprunghoehe, updateBreite, updateDecksbalkenhoehe, updatePrecision }) => (
  <div>
    <div>
      <Field onChange={updateSprunghoehe} name="sprunghoehe" component={renderTextField} label="Sprunghöhe" error="Nur Zahlen erlaubt!" />
    </div>
    <div>
      <Field onChange={updateBreite} name="breite" component={renderTextField} label="Breite" error="Nur Zahlen erlaubt!" />
    </div>
    <div>
      <Field onChange={updateDecksbalkenhoehe} name="decksbalkenhoehe" component={renderTextField} label="Decksbalkenhöhe" error="Nur Zahlen erlaubt!" />
    </div>
    <div>
      <Field onChange={updatePrecision} name="precision" component={renderTextField} label="Genauigkeit" error="Nur Zahlen erlaubt!" />
    </div>
  </div>
);

Form.propTypes = {
  updateSprunghoehe: PropTypes.func.isRequired,
  updateBreite: PropTypes.func.isRequired,
  updateDecksbalkenhoehe: PropTypes.func.isRequired,
  updatePrecision: PropTypes.func.isRequired,
};

const BoatplankForm = connect(null, actions)(Form);

export default reduxForm({
  form: 'Form', // a unique identifier for this form
  validate: null,
})(BoatplankForm);
