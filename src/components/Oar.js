import React, { Component } from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import makerjs from 'makerjs';

class Oar extends Component {

  componentDidMount() {
    this.renderModel();
  }

  componentDidUpdate() {
    this.renderModel();
  }

  renderModel = () => {
    const { paths } = this.props;
    const svg = makerjs.exporter.toSVG(paths, { svgAttrs: { width: '100%', height: '300px' } });
    document.getElementById('maker-wrapper').innerHTML = svg;
  };

  render() {
    return <div id="maker-wrapper" style={this.props.style} />;
  }
}

Oar.propTypes = {
  style: stylePropType,
  paths: PropTypes.arrayOf(PropTypes.shape({
    types: PropTypes.string.isRequired,
    origin: PropTypes.arrayOf(PropTypes.number).isRequired,
    end: PropTypes.arrayOf(PropTypes.number).isRequired,
  })),
};

Oar.defaultProps = {
  style: {},
  paths: [],
};

export default Oar;
