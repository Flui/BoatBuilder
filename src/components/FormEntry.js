import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'material-ui/Slider';
import TextField from 'material-ui/TextField';

const FormEntry = ({ value, handleSlider, handleTextField, title, maxValue }) => (
  <div className="form-entry">
    <span
      style={{
        float: 'left',
        clear: 'both',
        marginTop: '22px',
        paddingLeft: '20px',
        width: '150px',
        textAlign: 'left',
      }}
    >
      {title}
    </span>
    <Slider
      min={1}
      max={maxValue}
      style={{ width: '200px', float: 'left', marginLeft: '20px' }}
      step={1}
      value={value}
      onChange={handleSlider}
    />
    <TextField
      id="text-field-controlled"
      style={{ float: 'left', marginLeft: '20px', marginTop: '6px', width: '50px' }}
      value={value}
      onChange={handleTextField}
    />
  </div>
);

FormEntry.propTypes = {
  value: PropTypes.number.isRequired,
  handleSlider: PropTypes.func.isRequired,
  handleTextField: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  maxValue: PropTypes.number.isRequired,
};

export default FormEntry;
