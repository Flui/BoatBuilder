/* eslint react/no-did-mount-set-state: 0 */
import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';
import DownloadIcon from 'material-ui/svg-icons/file/file-download';
import makerjs from 'makerjs';
import FormEntry from './FormEntry';
import Oar from './Oar';

class OarViewer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      edgeHeight: 10,
      deckheadHeight: 50,
      width: 1000,
      paths: [],
      bottomSide: false,
    };
  }

  componentDidMount() {
    this.calculatePaths();
  }

  getValueAt = (x) => {
    const { edgeHeight, width, deckheadHeight } = this.state;

    const alpha = (Math.PI * x) / width;
    const xk1 = Math.cos(alpha);
    const xk2 = 1 - ((2 * x) / width);
    const v = Math.sin(alpha);
    return (Math.sqrt(((xk1 - xk2) ** 2) + (v ** 2)) * edgeHeight) + deckheadHeight;
  };

  calculatePaths = (newValue) => {
    const state = { ...this.state, ...newValue };
    const { deckheadHeight, width, bottomSide } = state;
    const paths = [];
    const precision = width;
    let lastValue;
    const updateState = (newState) => {
      this.setState(newState);
    };
    setTimeout(() => {
      for (let i = 0; i <= precision; i += 1) {
        const x = (width * i) / precision;
        const y = this.getValueAt(x);
        if (lastValue) {
          paths[i - 1] = { type: 'line', origin: [lastValue.x, lastValue.y], end: [x, y] };
          if (bottomSide) {
            paths[precision + 1] = { type: 'line', origin: [0, 0], end: [width, 0] };
          } else {
            paths[(precision + i) - 1] = { type: 'line', origin: [lastValue.x, lastValue.y - deckheadHeight], end: [x, y - deckheadHeight] };
          }
        }
        lastValue = { x, y };
      }
      paths[bottomSide ? precision + 2 : 2 * precision] = { type: 'line', origin: [0, 0], end: [0, deckheadHeight] };
      paths[bottomSide ? precision + 3 : (2 * precision) + 1] = { type: 'line', origin: [width, 0], end: [width, deckheadHeight] };
      updateState({ paths });
    }, 1);
  };

  handleSHSlider = (event, value) => {
    this.setState({
      edgeHeight: value,
    });
    this.calculatePaths();
  };


  handleSHChange = (event) => {
    const val = event.target.value ? parseInt(event.target.value, 10) : 0;
    this.setState({ edgeHeight: val });
    this.calculatePaths({ edgeHeight: val });
  };

  handleDBHSlider = (event, value) => {
    this.setState({
      deckheadHeight: value,
    });
    this.calculatePaths({
      deckheadHeight: value,
    });
  };

  handleDBHChange = (event) => {
    const val = event.target.value ? parseInt(event.target.value, 10) : 0;
    this.setState({
      deckheadHeight: val,
    });
    this.calculatePaths({
      deckheadHeight: val,
    });
  };

  handleWSlider = (event, value) => {
    this.setState({
      width: value,
    });
    this.calculatePaths({
      width: value,
    });
  };

  handleWChange = (event) => {
    const val = event.target.value ? parseInt(event.target.value, 10) : 0;
    this.setState({
      width: val,
    });
    this.calculatePaths({
      width: val,
    });
  };

  handleBSToggle = () => {
    this.setState({
      bottomSide: !this.state.bottomSide,
    });
    this.calculatePaths({
      bottomSide: !this.state.bottomSide,
    });
  }

  downloadDXF = () => {
    const { edgeHeight, width, deckheadHeight, paths } = this.state;
    const rotPaths = paths.map((line) => {
      const origin = line.origin;
      origin.reverse();
      const end = line.end;
      end.reverse();
      return { ...line, origin, end };
    });

    const dxf = makerjs.exporter.toDXF(rotPaths);

    const element = document.createElement('a');
    element.setAttribute('href', `data:text/plain;charset=utf-8,${encodeURIComponent(dxf)}`);
    element.setAttribute('download', `${edgeHeight}-${width}-${deckheadHeight}.dxf`);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }


  render() {
    const { edgeHeight, deckheadHeight, width, paths, bottomSide } = this.state;
    // TODO add maxValue to config
    return (
      <div>
        <div style={{ width: '50%', float: 'left' }}>
          <FormEntry
            value={edgeHeight}
            maxValue={1000}
            handleSlider={this.handleSHSlider}
            handleTextField={this.handleSHChange}
            title="Sprunghöhe:"
          />
          <FormEntry
            value={width}
            maxValue={3000}
            handleSlider={this.handleWSlider}
            handleTextField={this.handleWChange}
            title="Breite:"
          />
          <FormEntry
            value={deckheadHeight}
            maxValue={500}
            handleSlider={this.handleDBHSlider}
            handleTextField={this.handleDBHChange}
            title="Decksbalkenhöhe:"
          />
          <div style={{ width: '205px' }}>
            <Toggle
              label="Unterseite gerade:"
              style={{
                marginLeft: '20px',
              }}
              toggled={bottomSide}
              onToggle={this.handleBSToggle}
            />
          </div>
        </div>
        <Oar
          style={{ width: '40%', float: 'right', margin: '24px' }}
          paths={paths}
        />
        <RaisedButton
          style={{ margin: '60px 20px' }}
          label="Download .DXF"
          icon={<DownloadIcon />}
          onClick={() => { this.downloadDXF(); }}
        />
      </div>
    );
  }
}


export default OarViewer;
