import React from 'react';
import AppBar from 'material-ui/AppBar';
import './App.css';
import OarViewer from './OarViewer';
import Menu from './Menu';

const App = () => (
  <div className="App">
    <AppBar
      className="header"
      title="Decksbalkenbuchtkonstruktion"
      iconClassNameRight="muidocs-icon-navigation-expand-more"
      iconElementLeft={<Menu />}
    />
    <OarViewer />
  </div>
);

export default App;
